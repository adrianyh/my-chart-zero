//
//  LoginForm.swift
//  MyChartZero
//
//  Created by Adrian YH on 2017-07-14.
//  Copyright © 2017 Sunnybrook. All rights reserved.
//

import UIKit

protocol LoginUserDelegate: class {
    func loginButton()
}

protocol SignUpUserDelegate: class {
    func signupButton()
}

class LoginForm: UITableViewCell {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    private weak var delegateLogin: LoginUserDelegate?
    private weak var delegateSignUp: SignUpUserDelegate?
    
    @IBAction func btnLoginClick(_ sender: Any) {
        delegateLogin?.loginButton()
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        delegateSignUp?.signupButton()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ delegateLogin: LoginUserDelegate, _ delegateSignUp: SignUpUserDelegate) {
        self.delegateLogin = delegateLogin
        self.delegateSignUp = delegateSignUp
        
    }
}
