//
//  LoginTableViewController.swift
//  MyChartZero
//
//  Created by Adrian YH on 2017-07-14.
//  Copyright � 2017 Sunnybrook. All rights reserved.
//

import UIKit

class LoginTableViewController: UITableViewController {
    
    fileprivate enum ScreenDetail: Int {
        case padding, logo, login
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        //self.title = "Login"
        print("Login")
        
        
        tableView.register(UINib(nibName: String(describing: PaddingTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: PaddingTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: LogoImage.self), bundle: nil), forCellReuseIdentifier: String(describing: LogoImage.self))
        tableView.register(UINib(nibName: String(describing: LoginForm.self), bundle: nil), forCellReuseIdentifier: String(describing: LoginForm.self))
        
        
        tableView.estimatedRowHeight = 350
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)

    }
/*
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch ScreenDetail(rawValue: indexPath.row)! {
        case .padding:
            return 30
        case .logo:
            return 300
        case .login:
            return 350
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        switch ScreenDetail(rawValue: indexPath.row)!{
        case .padding:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaddingTableViewCell.self), for: indexPath) as! PaddingTableViewCell
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        case .logo:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LogoImage.self), for: indexPath) as! LogoImage
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        case .login:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LoginForm.self), for: indexPath) as! LoginForm
            cell.configure(self, self)
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue){
        
    }

}

extension LoginTableViewController: LoginUserDelegate {
    func loginButton() {
        print("Login User")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navBarController = storyBoard.instantiateViewController(withIdentifier: "navBarMyChart") as! UINavigationController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = navBarController
        
    }
}

extension LoginTableViewController: SignUpUserDelegate {
    func signupButton() {
        self.performSegue(withIdentifier: "segRegisterView", sender: self)
    }
}
