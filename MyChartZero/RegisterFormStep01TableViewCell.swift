//
//  RegisterFormStep01TableViewCell.swift
//  MyChartZero
//
//  Created by Adrian YH on 2017-07-19.
//  Copyright © 2017 Sunnybrook. All rights reserved.
//

import UIKit

protocol SignInUserDelegate: class {
    func signInButton()
}

protocol RegisterUserDelegate: class {
    func registrationSubmitButton()
}

class RegisterFormStep01TableViewCell: UITableViewCell {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPasswordConfirm: UITextField!
    @IBOutlet weak var btnRegistrationSubmit: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    private weak var delegateSignIn: SignInUserDelegate?
    private weak var delegateRegister: RegisterUserDelegate?
    
    @IBAction func btnRegistrationSubmitClick(_ sender: Any) {
        delegateRegister?.registrationSubmitButton()
    }
    
    @IBAction func btnLoginClick(_ sender: Any) {
        delegateSignIn?.signInButton()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ delegateSignIn: SignInUserDelegate, _ delegateRegister: RegisterUserDelegate) {
        self.delegateSignIn = delegateSignIn
        self.delegateRegister = delegateRegister
        
    }
}
