//
//  RegisterTableViewController.swift
//  MyChartZero
//
//  Created by Adrian YH on 2017-07-14.
//  Copyright © 2017 Sunnybrook. All rights reserved.
//

import UIKit

class RegisterTableViewController: UITableViewController {

    fileprivate enum ScreenDetail: Int {
        case padding1, header, padding2, register
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        print("Register Screen")
        
        tableView.register(UINib(nibName: String(describing: HeaderTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: PaddingTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: PaddingTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: RegisterFormStep01TableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: RegisterFormStep01TableViewCell.self))
        
        
        tableView.estimatedRowHeight = 350
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch ScreenDetail(rawValue: indexPath.row)! {
        case .padding1:
            return 20
        case .header:
            return 44
        case .padding2:
            return 20
        case .register:
            return 450
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        switch ScreenDetail(rawValue: indexPath.row)!{
        case .padding1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaddingTableViewCell.self), for: indexPath) as! PaddingTableViewCell
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        case .header:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderTableViewCell.self), for: indexPath) as! HeaderTableViewCell
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        case .padding2:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaddingTableViewCell.self), for: indexPath) as! PaddingTableViewCell
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        case .register:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RegisterFormStep01TableViewCell.self), for: indexPath) as! RegisterFormStep01TableViewCell
            cell.configure(self, self)
            cell.backgroundColor = UIColor(red: 1, green: 204/255, blue: 0, alpha: 1)
            cell.selectionStyle = .none
            return cell
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterTableViewController: SignInUserDelegate {
    func signInButton() {
        self.performSegue(withIdentifier: "unwindToLogin", sender: nil)
    }
}

extension RegisterTableViewController: RegisterUserDelegate {
    func registrationSubmitButton() {
        print("Register User")
    }
}
